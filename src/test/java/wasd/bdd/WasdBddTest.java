package wasd.bdd;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import org.junit.runner.RunWith;


/**
 * @project wasd_test
 * Created by Andrey M on 30/01/2020.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "summary"},
		features = "src/test/resources/features",
		strict = true,
		glue = "wasd.bdd",
		snippets = SnippetType.CAMELCASE
)
public class WasdBddTest {

	public void bddScenarioTest() {
	}
}
