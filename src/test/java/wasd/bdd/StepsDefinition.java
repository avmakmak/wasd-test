package wasd.bdd;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideDriver;
import io.cucumber.java.ru.Дано;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;
import wasd.interview.driverManager.DriverManagerFactory;
import wasd.interview.driverManager.DriverType;
import wasd.interview.page.GameCardPage;
import wasd.interview.page.GamesPage;
import wasd.interview.page.MainPage;
import wasd.interview.page.VideoPage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.page;
import static org.assertj.core.api.Assertions.assertThat;


/**
 * @project wasd_test
 * Created by Andrey M on 30/01/2020.
 */
public class StepsDefinition {

	SelenideDriver driverManager;

	private static final String BASE_URL = "https://wasd.tv/";
	private static String gameCardName;
	private static String videoTitle;

	private <T> T thenAt(Class<T> tClass) {
		return page(tClass);
	}

	@Дано("^инициализация драйвера$")
	public void инициализацияДрайвера() {
		driverManager = DriverManagerFactory.getManager(DriverType.CANARY).getDriver(); // Сетап драйвера под браузер

		Configuration.baseUrl = BASE_URL;
		Configuration.pageLoadStrategy = "none";
		Configuration.startMaximized = true;
	}

	@Когда("^переход на сайт wasd.tv$")
	public void переходНаСтраницуWasdTv() {
		Selenide.open("", MainPage.class);

	}

	@Тогда("открыта главная страница {string}")
	public void открытаГлавнаяСтраница(String title) {
		//Открывает страницу и делает переход слишком быстро, поэтому ожидание на faq
		thenAt(MainPage.class).header().menuElement("FAQ").shouldBe(visible);
		assertThat(Selenide.title()).isEqualTo(title);
	}

	@Когда("нажать на {string}")
	public void нажатьНаКаталогИгр(String catalog) {
		thenAt(MainPage.class).header().menuElement(catalog).click();
	}

	@Тогда("заголовок страницы является {string}")
	public void заголовокСтраницыЯвляетсяИгры(String title) {
		thenAt(GamesPage.class).title().shouldHave(text(title));
	}

	@Когда("нажать на карточку игры <a>, следующую за {string}")
	public void нажатьНаКарточкуИгрыAСледующуюЗаOtherGamesDemos(String cardName) {
		GamesPage gamesPage = thenAt(GamesPage.class);

		gameCardName = gamesPage.nextGameCardNameToText(cardName);
		gamesPage.nextGameCardTo("Other Games + Demos").click();
	}

	@Тогда("^заголовок в карточке игры является <a>$")
	public void заголовокВКарточкеИгрыЯвляетсяA() {
		thenAt(GameCardPage.class).gameCardTitle().shouldHave(text(gameCardName));
	}

	@Когда("^нажать на первый вод стрима$")
	public void нажатьНаПервыйВодСтрима() {
		GameCardPage gameCardPage = thenAt(GameCardPage.class);

		videoTitle = gameCardPage.videoTitlesList().first().getText();
		gameCardPage.videosList().first().click();
	}

	@Тогда("^открыта страница вода с заголовком является <b>$")
	public void открытаСтраницаВодаСЗаголовкомЯвляетсяB() {
		thenAt(VideoPage.class).videoTitle().shouldHave(text(videoTitle));
	}

	@Когда("^открыть настройки чата$")
	public void открытьНастройкиЧата() {
		thenAt(VideoPage.class).chat().settingsMenu().click();
	}

	@И("нажать на тумблер {string}")
	public void нажатьНаТумблерТемнаяТема(String chatSetting) {
		thenAt(VideoPage.class).chat().enableSetting(chatSetting);
	}

	@Тогда("в чате включена {string}")
	public void чатПереведёнВТемнуюТему(String chatSetting) {
		VideoPage videoPage = thenAt(VideoPage.class);

		assertThat(videoPage.chat().chatSettingIsEnabled(chatSetting)).isTrue();
	}

	//Да, здесь без аллюра
}
