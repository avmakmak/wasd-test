package wasd.interview;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideDriver;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import wasd.interview.driverManager.DriverManagerFactory;
import wasd.interview.driverManager.DriverType;
import wasd.interview.page.GameCardPage;
import wasd.interview.page.GamesPage;
import wasd.interview.page.MainPage;
import wasd.interview.page.VideoPage;

import java.io.IOException;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;
import static org.assertj.core.api.Assertions.assertThat;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class WasdTest {

	SelenideDriver driverManager;

	private static final String BASE_URL = "https://wasd.tv/";
	private static String gameCardName;
	private static String videoTitle;

	@BeforeEach
	public void config() {
		driverManager = DriverManagerFactory.getManager(DriverType.CANARY).getDriver(); // Сетап драйвера под браузер

		Configuration.baseUrl = BASE_URL;
		Configuration.pageLoadStrategy = "none";
		Configuration.startMaximized = true;

		SelenideLogger.addListener("AllureSelenide",
		                           new AllureSelenide().screenshots(true).savePageSource(false));
	}

	@Test
	public void testInterview() {
		MainPage mainPage = open("", MainPage.class);
		GamesPage gamesPage = page(GamesPage.class);
		GameCardPage gameCardPage = page(GameCardPage.class);
		VideoPage videoPage = page(VideoPage.class);

		mainPage.header().menuElement("Каталог игр").click();
		gamesPage.title().shouldHave(text("Игры"));

		gameCardName = gamesPage.nextGameCardNameToText("Other Games + Demos");
		gamesPage.nextGameCardTo("Other Games + Demos").click();

		gameCardPage.gameCardTitle().shouldHave(text(gameCardName));
		videoTitle = gameCardPage.videoTitlesList().first().getText();
		gameCardPage.videosList().first().click();

		videoPage.videoTitle();
		videoPage.videoTitle().shouldHave(text(videoTitle));
		videoPage.chat().settingsMenu().click();
		videoPage.chat().enableSetting("Темная тема");

		assertThat(videoPage.chat().chatSettingIsEnabled("Темная тема")).isTrue();
	}

	@AfterAll
	public static void tearDown() {
		buildReport();
	}

	private static void buildReport() {
		String os = System.getProperty("os.name");

		ProcessBuilder processBuilder = new ProcessBuilder();

		if(os.contains("Windows")) {
			processBuilder.command("cmd.exe", "/c", "allure serve target/allure-results");
		} else if (os.contains("Linux")) {
			processBuilder.command("bash", "-c", "allure serve target/allure-results");
		} else if (os.contains("Mac")) {
			processBuilder.command("/bin/bash", "-c", "allure serve target/allure-results");
		}

		try {
			processBuilder.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Selenide.closeWebDriver();
	}
}
