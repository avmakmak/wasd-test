package wasd.interview.page;

import com.codeborne.selenide.ElementsCollection;
import wasd.interview.layout.WithHeader;

import static com.codeborne.selenide.Selenide.$$x;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class MainPage extends WithHeader {

	public ElementsCollection popularGamesList() {
		return $$x("//img[@class='card-game__img visible card-game__all']");
	}
}
