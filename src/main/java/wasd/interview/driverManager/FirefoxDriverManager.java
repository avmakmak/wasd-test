package wasd.interview.driverManager;

import com.codeborne.selenide.Configuration;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class FirefoxDriverManager extends DriverManager{

	@Override
	protected void setConfig() {
		Configuration.browser = "firefox";
	}
}
