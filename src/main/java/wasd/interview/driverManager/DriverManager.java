package wasd.interview.driverManager;

import com.codeborne.selenide.SelenideDriver;
import com.codeborne.selenide.WebDriverRunner;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public abstract class DriverManager {

	protected SelenideDriver driver;
	protected abstract void setConfig();

	public SelenideDriver getDriver() {
		setConfig();

		return driver;
	}

	public void closeDriver() {
		WebDriverRunner.closeWebDriver();
	}
}
