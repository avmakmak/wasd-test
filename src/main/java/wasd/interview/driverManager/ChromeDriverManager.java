package wasd.interview.driverManager;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.chrome.ChromeOptions;


/**
 * @project wasd_test
 * Created by Andrey M on 28/01/2020.
 */
public class ChromeDriverManager extends DriverManager {

	@Override
	protected void setConfig() {
		Configuration.browser = "chrome";

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation");
		options.addArguments("--disable-gpu");

		Configuration.browserCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
	}
}
